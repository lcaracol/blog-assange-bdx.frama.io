[19/09/2023] [Bordeaux | Utopia] Projection de "The War on journalism"
################################################################################

:date: 2023-09-19 20:30
:category: projections

Projection à l'Utopia à Bordeaux le 19 septembre à 20h30

Entrée 4 euros

The War on journalism (J.Passarelli 2020)

https://www.cinemas-utopia.org/bordeaux/index.php?id=7106&mode=film

https://www.club-presse-bordeaux.fr/evenement/projection-debat-the-war-on-journalism-the-case-of-julian-assange-a-lutopia/
