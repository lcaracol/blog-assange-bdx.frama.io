[22/01/2024] Publication des .pdf des Flyers sur le site d'Aquilenet
################################################################################

:date: 2024-01-22 08:22
:category: Flyers-Affiches-etc


Annonce sur la liste de diffusion bistro@aquilenet.fr, de la
publication des .pdf des Flyers sur le site d'Aquilenet.

Les fichiers sont disponibles au téléchargement à l'adresse suivante:

https://cloud.aquilenet.fr/s/PH9PoR5s5BxFrTt

