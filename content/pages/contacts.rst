Contacts
################################################################################

:page_order: 4000


Les canaux de communication publics qu'il sera possible
de suivre,
auxquels il sera possible de s'inscrire,
pour suivre l'actualité de la mobilisation en ce qui concerne Bordeaux.

Attention.
Il ne s'agit que des organisations de Bordeaux ou en Gironde.
Les organisations nationales ne figurent pas sur cette page,
elles peuvent être trouvées sur la page
`Participants <{filename}participants.rst>`_
à la
section
`Organisations Nationales <participants.html#orgas-nationales>`_

.. contents::  .
 :depth: 5
 
.. sectnum:: 

------------------------------------------------------------------------------


Chat: IRC / Xmpp / Matrix etc...
================================================================================

Les outils de chats en temps réel disponibles.
Plusieurs outils différents sont disponible,
qui utilisent plusieurs protocoles,
et demandent plus ou moins de connaissances techniques
pour être utilisés.
Il y a IRC, Xmpp, Matrix, qui utilisent des logiciels libres.
Il peut aussi éventuellement y avoir des outils
mais moins respectueux de la vie privée,
mais beaucoup plus simples d'utilisation
et dont l'usage est beaucoup plus répandu.

IRC: Internet_Relay_Chat
------------------------------------------------------------------------------
Un protocole assez ancien, qui peut aussi être utilisé
facilement
avec un navigateur web.

https://fr.wikipedia.org/wiki/Internet_Relay_Chat

chat IRC d'Aquilenet
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

ircs://#aquilenet@irc.libera.chat

Il est possible de participer aux discussions du chat irc 
d'Aquilenet
en utilisant l'interface web fournie par libera.chat:

    https://web.libera.chat/gamja/?join=#aquilenet

Il est aussi possible d'utiliser l'interface web fournie par le
site d'Aquilenet:

    http://tools.aquilenet.fr/irc.html


Fils d'Information:
================================================================================

Il est possible de recevoir des informations
en recevant directement
des email,
en s'abonnant à des listes de diffusions
et des newsletters.
Il est également possible
de recevoir les informations en utilisant
un Newsreader
(un logiciel de lecture de flux .rss),
en s'abonnant à des flux .rss 

La diffusion des informations
n'étant pas homogène,
il convient de suivre aussi les publications
des réseaux sociaux
de chacune des organisations participantes.
Il n'y a
en effet
pour l'instant aucun flux
qui rassemble l'ensemble
de manière instantannée.

Listes de Diffusion par Mail publiques et Newsletters
------------------------------------------------------------------------------

Il est possible de s'inscrire à ces listes de discussion
publiques en remplissant un formulaire.
Pour cela il suffit de cliquer
sur le lien
vers la page web qui est donnée pour chacune des listes.

bistro@listes.aquilenet.fr
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
La page d'inscription à la mailing-list de discussions
d'Aquilenet:

    https://listes.aquilenet.fr/subscribe/bistro

blahblah@listes.abul.org
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
La page d'inscription à la mailing-list de discussions
de l'Abul:

    https://listes.abul.org/subscribe/blahblah

blog-assange-bdx@framagroupes.org
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
La page d'inscription à la mailing-list de discussions
de ce blog:

    https://framagroupes.org/sympa/info/blog-assange-bdx

Newsreader: Feed .rss .atom
------------------------------------------------------------------------------
Pour être tenu au courant des nouvelles publications
de divers sites webs,
il est possible de s'abonner
à des flux .rss ou .atom .
En utilisant un lecteur de flux .rss
(Outlook, Liferea, Thunderbird, etc...)

Les liens pour suivre les publications des sites suivants
peuvent être utilisés
par un lecteur de flux .rss :

.rss Free Assange Wave Bordeaux
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Il est possible d'être averti des nouvelles publications
de la page mastodon de Free Assange Wave Bordeaux
en s'abonnant à ce flux .rss:

    https://defcon.social/@freeassangewavebordeaux.rss



.rss Blog Assange Bordeaux
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Il est possible d'être averti des nouvelles publications
du blog blog-assange-bdx.frama.io
en s'abonnant soit à un flux .rss,
soit à un flux .atom:

https://blog-assange-bdx.frama.io/atom.xml

https://blog-assange-bdx.frama.io/rss.xml


Réseaux Sociaux
================================================================================

Mastodon
------------------------------------------------------------------------------

Mastodon est une alternative à Twitter et Facebook

Mastodon de Free Assange Wave Bordeaux
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
La page du profil mastodon de Free Assange Wave Bordeaux
relaye en direct l'actualité de cette mobilisation:

    https://defcon.social/@freeassangewavebordeaux

Il est possible de s'abonner au flux .rss pour être notifié
des nouvelles publications du profil.

Twitter
------------------------------------------------------------------------------


Facebook
------------------------------------------------------------------------------

Ligue des Droits de l'Homme Bordeaux
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Les actualités de la section bordelaise de la LDH
sont annoncées sur leur profil Facebook,
sur cette page:

    https://www.facebook.com/LDHBordeauxGironde/


Sites Web Traditionnels
================================================================================

En dehors des pages de profils des réseaux sociaux,
il est aussi possible de se rendre sur le site web
des organisations qui n'utilisent pas de réseaux sociaux,
mais utilise le site web principal,
pour afficher leurs actualités.

Organisations participantes
------------------------------------------------------------------------

Amnesty bordeaux
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
En se qui concerne Amnesty International,
pour suivre les publications de la section bordelaise
nous n'avons trouvé que leur site web.

    https://www.amnesty.fr/pres-de-chez-vous/bordeaux-sud

Ceci dit, leur site donne une adresse mail de contact,
bordeauxsud@amnestyfrance.fr ,
et surtout une page instagram,
où il sera en effet possible
de suivre leurs actualités:

    https://www.instagram.com/amnesty_bordeaux_33/?hl=fr 
