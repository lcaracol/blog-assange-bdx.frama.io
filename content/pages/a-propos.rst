À Propos
################################################################################

.. ~ Présentation de ce Blog

.. ~ :date: 2024-02-29
.. ~ :category: Présentation du Site
.. ~ :summary: Présentation du Site
.. ~ :pin: True

:page_order: 7000

.. contents::  .
 :depth: 2

------------------------------------------------------------------------------

Ce blog est une initiative
-non officielle-
visant à rassembler des liens vers les
différents sites des différents acteurs de la mobilisation à Bordeaux
pour le refus de l'extradition et la libération de Julian Assange.


Rassembler et Relayer des Informations au Sujet de la Mobilisation
------------------------------------------------------------------------

L'objet premier est donc de
rassembler les liens
vers les différents canaux de comunication
disponibles pour participer à cette mobilisation.

De manière à pouvoir présenter un suivi des différentes étapes
de la mobilisation à Bordeaux contre l'extradition de Julian Assange.

Et relayer/rassembler/annoncer sur un flux .rss
lisible par logiciel Newsreader
les principales annonces faites par les différentes organisations
participant à cette mobilisation.

L'objectif second est donc de créer ce flux .rss
permettant de suivre,
avec parfois un délai,
l'ensemble des acteurs.

Mais il est surtout fortement recommandé de suivre aussi directement ces acteurs,
pour être tenu au courant plus rapidement.

Publier des Évènements
------------------------------------------------------------------------

Ce site ne constitue pas en lui même une plateforme de publication d'évènements.
Car il y aurait un délai avant que nous ne puissions mettre le site à jour.
La mise à jour est manuelle, et ne se fait pas tous les jours.

Pour publier l'annonce d'une évènement, d'une projection ou autres,
nous recommanderions une plateforme de publication,
par exemple
`Démosphère <https://gironde.demosphere.net/search?search=Assange>`_ ,
mais quel que soit
le site ayant servi à faire l'annonce, Facebook, etc...
nous serons en mesure de rajouter le lien sur notre site.



Pour que nous puissions le relayer,
le plus simple est de nous envoyer
dans un email
le lien
qui dirige
vers la page web
de publication de l'évènement.
En envoyant un e.mail à la liste de diffusion
blog-assange-bdx@framagroupes.org .
(Pour qu'il soit diffusé,
n'oubliez pas de répondre au message automatique
demandant confirmation de l'envoi).

Contacts: Annuaire des Association Participantes Locales
------------------------------------------------------------------------
Pour rejoindre des Chats, Groupes de Discussion, et s'abonner
aux fils d'information, une liste a été rassemblée sur la page
"Contacts".
`"Contacts" <{filename}contacts.rst>`_.


C'est ce que nous vous recommandons,
si vous souhaitez être tenu au courant
sans délai des annonces qui seront faites.

La liste des participants et partenaires qui nous avons recensés 
est consultable à la page
`"Participants" <{filename}participants.rst>`_

Flyers, Stickers, Affiches, etc...
------------------------------------------------------------------------
Les liens vers des fichiers imprimables,
et les informations pour récupérer
flyers/stickers/affiches/etc imprimés,
sont rassemblés dans la page "Stickers-Flyers-etc".

Contenu des Billets
------------------------------------------------------------------------
Le format de blog permettra de publier
sous la forme de billets
les annonces de rencontres,
de projections et autres.

Mais le détail des annonces
devra être consulté directement
sur les pages d'annonce des organisations
participantes.

Il n'y aura ici pas de comptes rendus détaillés,
ni autre articles,
afin de pouvoir se concentrer principalement sur le déroulement.
Et évidement,
encourager
le renvoi vers les sites des organisations participantes
et partenaires.

On se cantonnera ici aux années 2023 et 2024.

.. _autres-villes:

Pour Les Autres Villes
------------------------------------------------------------------------
Si vous souhaitez aussi monter un blog semblable
pour votre ville, nous n'avons aucun problème
à ce que vous récupériez des éléments présent
sur ce site,
au contraire,
il est fait pour ça.
Nous vous y encourageons

Le code source du site est publié sur framagit
en suivant ce
lien: <https://framagit.org/blog-assange-bdx/blog-assange-bdx.frama.io/-/tree/master/content?ref_type=heads>
, vous pouvez donc le forker sur Framagit.
Ou vous pouvez aussi simplement
récupérer l'archive .zip pour vous, 
https://framagit.org/blog-assange-bdx/blog-assange-bdx.frama.io/-/archive/master/blog-assange-bdx.frama.io-master.zip
et l'éditer selon vos besoins.

Une grande partie des fichiers des affiches et flyers
peuvent être directement réutilisables,
et pour les autres, il y a des fichiers éditables,
qui permettront par exemple
de remplacer notre QR-code
par le QR-code de l'url de votre site.

N'hésitez pas.
Le site est réalisé avec le générateur de site statique
`Pelican <https://docs.getpelican.com/en/stable/content.html#writing-content>`_ ,
les explications sont dans le fichier README.md dans le répertoire du projet.
Il suffit d'éditer les fichiers au format .rst
du dossier **content**,
qui utilisent la syntaxe Restructured Text, 
modifier le fichier pelicanconf.py
avec vos paramètres,
et mettre vos propres versions des flyers.
S'il y a un groupe d'utilisateur Linux dans votre ville,
ils devraient pouvoir vous y aider facilement.

En particulier, pour faire rapidement
vos propres affiches et flyers,
en réutilisant les notres, 
c'est expliqué sur cette partie de la page stickers-flyers-etc.html:
`lien "Adapter les Flyers" <./stickers-flyers-etc.html#reutiliser-flyers-affiches>`_

 .. note :: Notes: Site En Construction

      Il reste encore des imperfections sur le présent site,
      il n'est pas encore terminé.

